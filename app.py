from flask import Flask,request,jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

# Run app
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
# Data base
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///'+os.path.join(basedir,'db.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] =False
# Init db
db = SQLAlchemy(app)
# Init masrsh
ma = Marshmallow(app)

# Class product (DB)
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100),unique=True)
    brand = db.Column(db.String(100))
    description = db.Column(db.String(500))
    currentprice = db.Column(db.Float)
    bestprice = db.Column(db.Float)
    #new
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    location = db.Column(db.String(500))

    # Constructor
    def __init__(self,name,brand,description,currentprice,bestprice,latitude,longitude,location):
        self.name = name
        self.brand = brand
        self.description = description
        self.currentprice = currentprice
        self.bestprice = bestprice
        self.latitude = latitude
        self.longitude = longitude
        self.location = location

# Schema pruduct (marsh)
class ProductSchema(ma.Schema):
    class Meta:
        fields =('id','name','brand','description','currentprice','bestprice','latitude','longitude','location')
# Init schema
# strict = True
product_schema = ProductSchema()
products_schema = ProductSchema(many=True)

# CREAR PRODUCT (POST)
@app.route('/product',methods=['POST'])
def add_product():
    name = request.json['name']
    brand = request.json['brand']
    description = request.json['description']
    currentprice = request.json['currentprice']
    bestprice = request.json['bestprice']
    latitude = request.json['latitude']
    longitude = request.json['longitude']
    location = request.json['location']

    new_product = Product(name,brand,description,currentprice,bestprice,latitude,longitude,location)
    db.session.add(new_product)
    db.session.commit()

    return product_schema.jsonify(new_product)

# OBTENIR PRODUCT (GET)
@app.route('/product',methods=['GET'])
def get_products():
    all_products = Product.query.all()
    result = products_schema.dump(all_products)
    return jsonify(result)

# OBTENIR 1 PRODUCT
@app.route('/product/<id>',methods=['GET'])
def get_product(id):
    product = Product.query.get(id)
    return product_schema.jsonify(product)

# MODIFY PRODUCT (PUT)
@app.route('/product/<id>',methods=['PUT'])
def modify_product(id):
    product = Product.query.get(id)
    name = request.json['name']
    brand = request.json['brand']
    description = request.json['description']
    currentprice = request.json['currentprice']
    bestprice = request.json['bestprice']
    latitude = request.json['latitude']
    longitude = request.json['longitude']
    location = request.json['location']


    product.name = name
    product.brand = brand
    product.description = description
    product.currentprice = currentprice
    product.bestprice = bestprice
    product.latitude = latitude
    product.longitude = longitude
    product.location = location
    db.session.commit()
    return product_schema.jsonify(product)

#DELETE PRODUCT (DELETE)
@app.route('/product/<id>',methods=['DELETE'])
def delete_product(id):
    product = Product.query.get(id)
    db.session.delete(product)
    db.session.commit()
    return product_schema.jsonify(message = 'product deleted')



# Run server
if __name__ == '__main__':
    app.run(debug=True)
